from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    picture = json.loads(response.content)
    picture_url = {
        "picture_url": picture["photos"][0]["url"]
    }
    return picture_url

def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},USA",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
    }
    r = requests.get(url, params=params)
    d = json.loads(r.content)
    lat = d[0]["lat"]
    lon = d[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=params)
    data = json.loads(response.content)
    print(data)
    weather = {
        "description": data["weather"][0]["description"],
        "weather": data["main"]["temp"]
    }
    return weather
